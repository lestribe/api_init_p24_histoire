function menu_end {
echo " "
echo " "
echo " "
echo "THE END"
echo " "
echo " "
echo "1. Recommencer"
echo "2. Quitter"

    read -p "Entrez votre choix : " choice
    if [[ $choice -eq 1 ]]; then
        ./menu.sh
    elif [[ $choice -eq 2 ]]; then
        exit
    else
        echo "Euh t'as pas compris je crois. Tu dois taper 1 ou 2 !"
        menu_end  # Retourne un code d'erreur en cas de choix invalide
    fi

}


# Appel de la fonction principale
#menu_end
#choice=$?
