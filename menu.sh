source bravo.sh
source fonction_affichage.sh
source fonction_choix.sh
source fonction_mort.sh
source fonction_redirection.sh
source choix_porte.sh
source fonction_fin.sh



function menu_start {
echo " "
echo " "
echo " "
echo "Menu Start"
echo " "
echo " "
echo " Bienvenue dans l'Histoire dont vous êtes le héros."
echo "Au cours de ce jeu, vous allez  être amenés à faire des choix difficiles..."
echo "Appuyez sur les bonnes touches et n'oubliez pas ! Surtout, restez en vie..."
echo "Si par malheur vous devez quitter le jeu (pour sauver le monde par exemple), faites ctrl+C "
echo " "
echo " "
echo "1. Commencer"
echo "2. Quitter"
echo " "
echo " "
echo " "
    read -p "Entrez votre choix : " choice
    if [[ $choice -eq 1 ]]; then
        affichage_niveau ./storyboard/01_A
    elif [[ $choice -eq 2 ]]; then
        exit
    else
        echo "Euh t'as pas compris je crois. Tu dois taper 1 ou 2 !"
        menu_start  # Retourne un code d'erreur en cas de choix invalide
    fi

}


# Appel de la fonction principale
menu_start
choice=$?
