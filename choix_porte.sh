# Fonction pour obtenir le choix de l'utilisateur
function choix_porte() {
    read -p "Choisissez entre 1, 2 et 3 : " CHOICE


    case $CHOICE in
        1)
            echo "Vous avez choisi 1. Si je vous dis que derrière 2 c'est l'enfer." >&2
            while true; do
                read -p "Voulez-vous choisir 3 à la place ? (oui/non) : " response
                if [[ "$response" == "oui" ]]; then
                    CHOICE=1  # L'utilisateur a changé de choix
                    break
                elif [[ "$response" == "non" ]]; then
                    CHOICE=2  
                    break
                else
                    echo "Réponse invalide. Veuillez répondre par oui ou non."
                fi
            done
            ;;
        2)
            echo "Vous avez choisi 2. Si je vous dis que derrière 3 c'est l'enfer." >&2
            while true; do
                read -p "Voulez-vous choisir 1 à la place ? (oui/non) : " response
                if [[ "$response" == "oui" ]]; then
                    CHOICE=1  # L'utilisateur a changé de choix
                    break
                elif [[ "$response" == "non" ]]; then
                    CHOICE=2 
                    break
                else
                    echo "Réponse invalide. Veuillez répondre par oui ou non."
                fi
            done
            ;;
        3)
            echo "Vous avez choisi 3. Si je vous dis que derrière 1 c'est l'enfer." >&2
            while true; do
                read -p "Voulez-vous choisir 2 à la place ? (oui/non) : " response
                if [[ "$response" == "oui" ]]; then
                    CHOICE=1  # L'utilisateur a changé de choix
                    break
                elif [[ "$response" == "non" ]]; then
                    CHOICE=2
                    break
                else
                    echo "Réponse invalide. Veuillez répondre par oui ou non."
                fi
            done
            ;;
        *)
            echo "Choix invalide. Veuillez entrer 1, 2 ou 3."
            CHOICE=$(get_user_choice)  # Réappelle la fonction si le choix est invalide
            ;;
    esac


}


