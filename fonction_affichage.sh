#Ceci est la fonction d'affichage, elle reçoit le nom d'un niveau et renvoie le texte qui lui est associé, l'image, et appelle la fonction choix

function affichage_niveau {
    niveau="$1" 	#niveau est un dossier

    timg "$1"/image* -gx20
    cat "$1"/texte.txt
	echo $1

	if [ -f "$1/mort" ]; then
		fonction_mort

	elif [ -f "$1/fin" ]; then
        	fonction_fin

	elif [ "$1" = ./storyboard/07_B ]; then
		choix_porte
	else
		choix
	fi
		chemin="$1"/choix_"$CHOICE".txt
		redirection "$chemin"


}

