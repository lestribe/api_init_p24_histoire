#!/bin/bash

# Fonction principale qui renvoie le choix de l'utilisateur
function choix() {
    read -p "Choisissez entre 1 et 2 : " CHOICE
    
    if [ $CHOICE -eq 1 ]; then
        echo "Vous avez choisi 1."
    elif [ $CHOICE -eq 2 ]; then
        echo "Vous avez choisi 2."
    else
        echo "Choix invalide. Veuillez entrer 1 ou 2."
        choix  # Retourne un code d'erreur en cas de choix invalide
    fi
    
}




